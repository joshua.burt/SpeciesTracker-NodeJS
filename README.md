# SpeciesTracker
concept biological catalogue <br>
https://speciestracker2.azurewebsites.net/

# To Start: <br>
Download zip, open in code editor <br>
In terminal: nodemon

# Capabilities <br>
CRUD, Multiple tables <br>
image upload: npm i multer <br>
date, time, gps autofill: npm/exif-js <br>
coordinates to map & map to coordinates: npm i leaflet

# Additions: <br>
*2 formats: <br>
*#A.# Your entries (log/journal); <br>
*on-click of image = enlarged version <br>
*search bar, order by: species, family, order, class, by time, alphabetic, location, usage/nutrition/toxicity -> at right of index partial <br>
*Add user login (user can only view own photos here)<br>
*#B.# Browse all users entries (with all locations & time on a single map ->if name same -->add to date, location, image array)<br>
*further information links: wiki, genetic profile, etc.; up-vote pictures <br>
*4 highest rated by morphology show in row -> on click other high rated photos <br>
<br>
*scientific name (genus & species) under name (use ai prompt on name creation?) <br>
*sub rows in one column ie. location and time sighted <br>
*add blog code to forum <br>
<br>
~Machine learning species identifier

